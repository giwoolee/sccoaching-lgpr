<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://sccoaching.com
 * @since             1.0.0
 * @package           Sccoaching_LGPR
 *
 * @wordpress-plugin
 * Plugin Name:       SCCoaching LGPR
 * Plugin URI:        http://sccoaching.com/lgpr/
 * Description:       MGSCC - Leadership Growth Progress Report
 * Version:           1.0.0
 * Author:            MG SCCoaching
 * Author URI:        https://www.linkedin.com/profile/preview?locale=en_US&trk=prof-0-sb-preview-primary-button
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sccoaching-lgpr
 * Domain Path:       /languages
 */

/* If this file is called directly, abort */
if ( !defined('WPINC') ) {
    die;
}

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * The code that runs during plugin activation
 * This action is documented in includes/Sccoaching_Lgpr_Activator.php
 */
function activate_plugin_sccoaching_lgpr() {
    require_once plugin_dir_path( __FILE__ ) . "includes/Sccoaching_Lgpr_Activator.php";
    Sccoaching_Lgpr_Activator::activate();
}

/*function install_plugin_sccoaching_lgpr() {
    require_once plugin_dir_path( __FILE__ );
}*/

/**
 * The code that run during plugin deactivation
 * This action is documented is includes/Sccoaching_Lgpr_Deactivator.php
 */
function deactivate_plugin_sccoaching_lgpr() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/Sccoaching_Lgpr_Deactivator.php';
    Sccoaching_Lgpr_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_plugin_sccoaching_lgpr' );
register_deactivation_hook( __FILE__, 'deactivate_plugin_sccoaching_lgpr' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/Sccoaching_Lgpr.php';

/**
 * Beings execution of the plugin
 *
 * Since everything within the plugin is reditered via hooks,
 * then kickin goff the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since 1.0.0
 */
function run_plugin_sccoaching_lgpr() {

    $plugin = new Sccoaching_Lgpr();
    $plugin->run();

}

run_plugin_sccoaching_lgpr();