(function($) {
    'use strict';

    /**
     * Selectors for DOM elements.
     *
     * @type {{searchForm: string, results: string}}
     */
    var selectors = {
            addSurveyForm: '#add_survey_form',
            editSurveyForm: '#edit_survey_form',
            editSurveyBtn: '.edit_survey_btn',
            deleteSurveyBtn: '.delete_survey_btn',
            parseCsvToJsonBtn: '#parse_csv_to_json_btn',
            responseSurvey: '#response_survey'
        },
        attrSelectors = {
            action: 'action'
        };

    $(function() {

        /**
         * ADD survey form submit.
         *
         * @param e
         * @returns {boolean}
         */
        $(selectors.addSurveyForm).submit(function(e) {
            // Prevent default event submission
            e.preventDefault();

            // Prepare AJAX call attributes
            var form = $(this),
                formData = form.serialize(),
                formMethod = form.attr('method'),
                responseMsg = $(selectors.responseSurvey);

            // Execute AJAX call
            $.ajax({
                url: AdminAjax.ajaxurl,
                data: formData,
                method: formMethod,
                success: function(data) {
                    if (data == "false") {
                        responseMsg.html("Error to insert new survey");
                    } else {
                        responseMsg.html("Survey successfully created");
                    }
                }
            });

            // Prevent form from submitting
            return false;
        });

        /**
         * DELETE survey form submit.
         *
         * @param e
         * @returns {boolean}
         */
        $(selectors.deleteSurveyForm).click(function(e) {
            // Prevent default event submission
            e.preventDefault();

            // Prepare AJAX call attributes
            var engagementId = $(this).data('id'),
                responseMsg = $(selectors.responseSurvey);

            // Execute AJAX call
            $.ajax({
                url: AdminAjax.ajaxurl,
                data: {
                    action: 'sccoaching_lgpr_delete_survey_form',
                    id: engagementId
                },
                method: 'POST',
                success: function(data) {
                    if (data == "false") { // if false (error)
                        responseMsg.html("Error to delete.");
                    } else {
                        location.reload();
                    }
                }
            });
        });

        /**
         * EDIT survey button
         *
         * @param e
         */
        $(selectors.editSurveyBtn).click(function(e) {

            e.preventDefault();

            // Retrieve data from the row
            var id = $(this).data("id"),
                name = $(this).data("name"),
                country = $(this).data("country");

            var query = "/edit";
            window.location = window.location.href.concat(query);

        });

        /**
         * EDIT parse CSV to JSON
         *
         * @param e
         */
        $(selectors.parseCsvToJsonBtn).fileupload(function(e) {

            e.preventDefault();

            console.log(e.target);

        });

        /**
         * EDIT survey form submit.
         *
         * @param e
         * @returns (boolean)
         */
        $(selectors.editSurveyForm).submit(function(e) {
            // Prevent form default event
            e.preventDefault();

            console.log("Edit Survey");
        });

    });

})(jQuery);