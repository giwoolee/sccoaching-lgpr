(function($) {
    'use strict';

    /**
     * Selectors for DOM elements.
     */
    var selectors = {
            addReportTemplateForm: '#add_report_template_form',
            addReportTemplateFormBtn: '.add_report_template_btn',
            formResponse: '#form_response'
        },
        attrSelectors = {
            action: 'action'
        };

    var formSelectors = {
        reportHeaderDescription: "#report-header-description",
        reportQuestionnaire: "#report-questionnaire",
        reportFooterCoaches: "#report-footer-coaches"
    };

    $(function() {
        /*console.log($(formSelectors.reportHeaderDescription).val());
        console.log($(formSelectors.reportQuestionnaire).val());
        console.log($(formSelectors.reportFooterCoaches).val());*/

        /**
         * Add/Edit report template to DB using AJAX.
         *
         * @param e
         * @returns {boolean}
         */
        $(selectors.addReportTemplateForm).submit(function(e) {
            // Prevent default event submission
            e.preventDefault();

            // Prepare AJAX call attributes
            var form = $(this),
                formData = form.serialize(),
                formMethod = form.attr('method'),
                formResponse = $(formResponse);

            // Execute AJAX call
            $.ajax({
                url: AdminAjax.ajaxurl,
                data: formData,
                type: formMethod,
                success: function(data, textStatus, jqXHR) {
                    if (typeof data.error === 'undefined') {
                        if (!data || data === "false" || data === false || data === 0 || data === '0') {
                            formResponse.addClass('alert alert-danger');
                            formResponse.html("Error attempting to update report template.");
                            console.log(data);
                        } else {
                            formResponse.addClass('alert alert-success');
                            formResponse.html("1 template successfully saved.");
                            console.log(data);
                        }
                    } else {
                        // Handle errors here
                        console.log("ERRORS: " + data.error);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // Handle errors here
                    console.log("ERRORS: " + textStatus);
                }
            });

            // Prevent form from submitting
            return false;
        });
    });

})(jQuery);