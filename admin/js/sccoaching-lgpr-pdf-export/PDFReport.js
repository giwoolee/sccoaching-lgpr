/**
 * Created by Gustavo on 3/2/2017.
 */
"use strict"

var fields = {
    templateRootDir: "../wp-content/plugins/sccoaching-lgpr/admin/partials/template/",
    targetPreFieldName: "lgpr-",
    reportTopBanner: "report-top-banner-img-uri",
    reportHeaderDescription: "report-header-description",
    reportLegendBanner: "report-legend-banner",
    reportDate: "report-date",
    reportCoach: "report-coach",
    reportQuestionnaire: "report-questionnaire",
    reportFooterCoaches: "report-footer-coaches",
    reportFooterCities: "report-footer-cities",
    reportFooterCountries: "report-footer-countries",
    reportFooterLanguages: "report-footer-languages",
    reportFooterProcess: "report-footer-process",
    reportFooterEmail: "report-footer-email",
    reportFooterWebsite: "report-footer-website"
};

function FieldUndefinedException(message) {
    this.message = message;
    this.name = 'FieldUndefinedException';
}

function PDFReport() {

    // init variables
    // iFrame
    var pdfFrame = document.getElementById("pdf_preview");
    this.innerDoc = pdfFrame.contentElement || pdfFrame.contentWindow.document;

    this.targetPreFieldName = fields.targetPreFieldName;
    this.defaultMgSccoachingBanner = fields.templateRootDir + "Web_Banner_950x174.jpg";

    // default properties
    // Report Top
    this.reportTopBanner = fields.reportTopBanner ? fields.reportTopBanner : undefined;
    this.reportHeaderDescription = fields.reportHeaderDescription ? fields.reportHeaderDescription : undefined;

    // Report Legend
    this.reportLegendBanner = fields.reportLegendBanner ? fields.reportLegendBanner : undefined;
    this.reportDate = fields.reportDate ? fields.reportDate : undefined;
    this.reportCoach = fields.reportCoach ? fields.reportCoach : undefined;

    // Report Questionnaire
    this.reportQuestionnaire = fields.reportQuestionnaire ? fields.reportQuestionnaire : undefined;

    // Report Footer
    this.reportFooterCoaches = fields.reportFooterCoaches ? fields.reportFooterCoaches : undefined;
    this.reportFooterCities = fields.reportFooterCities ? fields.reportFooterCities : undefined;
    this.reportFooterCountries = fields.reportFooterCountries ? fields.reportFooterCountries : undefined;
    this.reportFooterLanguages = fields.reportFooterLanguages ? fields.reportFooterLanguages : undefined;
    this.reportFooterProcess = fields.reportFooterProcess ? fields.reportFooterProcess : undefined;
    this.reportFooterEmail = fields.reportFooterEmail ? fields.reportFooterEmail : undefined;
    this.reportFooterWebsite = fields.reportFooterWebsite ? fields.reportFooterWebsite : undefined;

}

PDFReport.prototype.init = function() {
    var self = this;
    jQuery("#" + fields.reportTopBanner).change(function() {
        self.setReportTopBanner(false);
    });

    jQuery("#" + fields.reportLegendBanner).change(function() {
        self.setReportLegendBanner(false);
    });
};

/* Sets the header banner image */
PDFReport.prototype.setReportTopBanner = function(useDefaultHeader) {
    // image reading
    // More info at https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL
    // for more examples about file api
    // take a look at https://scotch.io/tutorials/use-the-html5-file-api-to-work-with-files-locally-in-the-browser

    // Copy the context from when this object is instantiated
    var self = this;

    // init variables
    var user_img = new Image(),
        img_data = null;

    // If reportTopBanner is empty then use one from the main object
    var reportTopBanner = this.reportTopBanner || undefined;

    // If headerImg is empty use the default one
    if (reportTopBanner) {

        var $top_banner_img = jQuery("#" + reportTopBanner);
        var top_header_banner = $top_banner_img[0].files[0];

        if (top_header_banner) {

            img_data = {
                type: top_header_banner.type === 'image/jpeg' ? 'JPEG' : 'PNG'
            };

            var reader = new FileReader();
            reader.onload = function (event) {
                img_data.src = event.target.result;

                user_img.onload = function () {
                    img_data.w = user_img.width;
                    img_data.h = user_img.height;
                };
                user_img.src = img_data.src;

                // Assigning new image
                var pdfFrame = document.getElementById("pdf_preview");
                var innerDoc = pdfFrame.contentElement || pdfFrame.contentWindow.document;
                innerDoc.getElementById(self.targetPreFieldName + reportTopBanner).setAttribute('src', user_img.src);
            };

            // when the file is read it triggers the onload event above.
            reader.readAsDataURL(top_header_banner);

        }

    } else if (useDefaultHeader) {

        user_img.src = this.defaultMgSccoachingBanner;
        user_img.width = '100';

    } else {

        throw FieldUndefinedException(reportTopBanner);

    }

};

/* Sets the report header description */
PDFReport.prototype.setReportHeaderDescription = function(reportHeaderDescription){

    // If reportHeaderDescription is empty use is from the main object
    reportHeaderDescription = this.reportHeaderDescription || undefined;

    if (reportHeaderDescription) {
        // Getting text content from source
        var sourceTxt = jQuery("#" + reportHeaderDescription).val();

        // Assigning string to template
        this.innerDoc.getElementById(this.targetPreFieldName + reportHeaderDescription).innerHTML = sourceTxt.trim();

    } else {
        throw FieldUndefinedException(reportHeaderDescription);
    }
};

/* Sets the legend image */
PDFReport.prototype.setReportLegendBanner = function(useDefaultHeader) {

    // Copy the context from when this object is instantiated
    var self = this;

    // init variables
    var user_img = new Image(),
        img_data = null;

    // If topBanner is empty then use one from the main object
    var reportLegendBanner = this.reportLegendBanner || undefined;

    // If headerImg is empty use the default one
    if (reportLegendBanner) {

        var $legend_banner_img = jQuery("#" + reportLegendBanner);
        var legend_banner_file = $legend_banner_img[0].files[0];

        if (legend_banner_file) {
            img_data = {
                type: legend_banner_file.type === 'image/jpeg' ? 'JPEG' : 'PNG'
            };

            var reader = new FileReader();
            reader.onload = function (event) {
                img_data.src = event.target.result;

                user_img.onload = function () {
                    img_data.w = user_img.width;
                    img_data.h = user_img.height;
                };
                user_img.src = img_data.src;

                // Assigning string to template
                var pdfFrame = document.getElementById("pdf_preview");
                var innerDoc = pdfFrame.contentElement || pdfFrame.contentWindow.document;
                innerDoc.getElementById(self.targetPreFieldName + reportLegendBanner).setAttribute('src', user_img.src);
            };

            // when the file is read it triggers the onload event above.
            reader.readAsDataURL(legend_banner_file);
        }

    } else if (useDefaultHeader) {
        user_img.src = this.defaultMgSccoachingBanner;
    } else {
        throw FieldUndefinedException(reportLegendBanner);
    }

};

/* Sets the report date */
PDFReport.prototype.setReportDate = function(reportDate){

    // If headerTitle is empty use is from the main object
    reportDate = this.reportDate || undefined;

    if (reportDate) {
        // Getting text content from source
        var sourceTxt = jQuery("#" + reportDate).val();

        // Assigning string to template
        this.innerDoc.getElementById(this.targetPreFieldName + reportDate).innerHTML = sourceTxt.trim();

    } else {
        throw FieldUndefinedException(reportDate);
    }
};

/* Sets the report coach */
PDFReport.prototype.setReportCoach = function(reportCoach) {
    // If headerTitle is empty use is from the main object
    reportCoach = this.reportCoach || undefined;

    if (reportCoach) {
        // Getting text content from source
        var sourceTxt = jQuery("#" + reportCoach).val();

        // Assigning string to template
        this.innerDoc.getElementById(this.targetPreFieldName + reportCoach).innerHTML = sourceTxt.trim();

    } else {
        throw FieldUndefinedException(reportCoach);
    }
};

/* Sets the report questionnaire */
PDFReport.prototype.setReportQuestionnaire = function(reportQuestionnaire){

    // If reportQuestionnaire is empty use is from the main object
    reportQuestionnaire = this.reportQuestionnaire || undefined;

    if (reportQuestionnaire) {
        // Getting text content from source
        var sourceTxt = jQuery("#" + reportQuestionnaire).val();

        // Assigning string to template
        this.innerDoc.getElementById(this.targetPreFieldName + reportQuestionnaire).innerHTML = sourceTxt.trim();

    } else {
        throw FieldUndefinedException(reportQuestionnaire);
    }
};

/* Sets the report footer coaches */
PDFReport.prototype.setReportFooterCoaches = function(reportFooterCoaches){

    // If reportFooterCoaches is empty use is from the main object
    if (!reportFooterCoaches)
        reportFooterCoaches = this.reportFooterCoaches;
    else if (!reportFooterCoaches && !this.reportFooterCoaches)
        reportFooterCoaches = undefined;

    if (reportFooterCoaches) {
        // Getting text content from source
        var sourceTxt = jQuery("#" + reportFooterCoaches).val();

        // Assigning string to template
        this.innerDoc.getElementById(this.targetPreFieldName + reportFooterCoaches).innerHTML = sourceTxt.trim();

    } else {
        throw FieldUndefinedException(reportFooterCoaches);
    }
};

/* Sets the report footer cities */
PDFReport.prototype.setReportFooterCities = function(reportFooterCities){

    // If reportFooterCoaches is empty use is from the main object
    reportFooterCities = this.reportFooterCities || undefined;

    if (reportFooterCities) {
        // Getting text content from source
        var sourceTxt = jQuery("#" + reportFooterCities).val();

        // Assigning string to template
        this.innerDoc.getElementById(this.targetPreFieldName + reportFooterCities).innerHTML = sourceTxt.trim();

    } else {
        throw FieldUndefinedException(reportFooterCities);
    }

};

/* Sets the report footer countries */
PDFReport.prototype.setReportFooterCountries = function(reportFooterCountries){

    // If reportFooterCountries is empty use is from the main object
    reportFooterCountries = this.reportFooterCountries || undefined;

    if (reportFooterCountries) {
        // Getting text content from source
        var sourceTxt = jQuery("#" + reportFooterCountries).val();

        // Assigning string to template
        this.innerDoc.getElementById(this.targetPreFieldName + reportFooterCountries).innerHTML = sourceTxt.trim();

    } else {
        throw FieldUndefinedException(reportFooterCountries);
    }

};

/* Sets the report footer languages */
PDFReport.prototype.setReportFooterLanguages = function(reportFooterLanguages){

    // If reportFooterLanguages is empty use is from the main object
    reportFooterLanguages = this.reportFooterLanguages || undefined;

    if (reportFooterLanguages) {
        // Getting text content from source
        var sourceTxt = jQuery("#" + reportFooterLanguages).val();

        // Assigning string to template
        this.innerDoc.getElementById(this.targetPreFieldName + reportFooterLanguages).innerHTML = sourceTxt.trim();

    } else {
        throw FieldUndefinedException(reportFooterLanguages);
    }

};

/* Sets the report footer process */
PDFReport.prototype.setReportFooterProcess = function(reportFooterProcess){

    // If reportFooterProcess is empty use is from the main object
    reportFooterProcess = this.reportFooterProcess || undefined;

    if (reportFooterProcess) {
        // Getting text content from source
        var sourceTxt = jQuery("#" + reportFooterProcess).val();

        // Assigning string to template
        this.innerDoc.getElementById(this.targetPreFieldName + reportFooterProcess).innerHTML = sourceTxt.trim();

    } else {
        throw FieldUndefinedException(reportFooterProcess);
    }

};

/* Sets the report footer email */
PDFReport.prototype.setReportFooterEmail = function(reportFooterEmail){

    // If reportFooterEmail is empty use is from the main object
    reportFooterEmail = this.reportFooterEmail || undefined;

    if (reportFooterEmail) {
        // Getting text content from source
        var sourceTxt = jQuery("#" + reportFooterEmail).val();

        // Assigning string to template
        this.innerDoc.getElementById(this.targetPreFieldName + reportFooterEmail).innerHTML = sourceTxt.trim();

    } else {
        throw FieldUndefinedException(reportFooterEmail);
    }

};

/* Sets the report footer website */
PDFReport.prototype.setReportFooterWebsite = function(reportFooterWebsite){

    // If reportFooterWebsite is empty use is from the main object
    reportFooterWebsite = this.reportFooterWebsite || undefined;

    if (reportFooterWebsite) {
        // Getting text content from source
        var sourceTxt = jQuery("#" + reportFooterWebsite).val();

        // Assigning string to template
        this.innerDoc.getElementById(this.targetPreFieldName + reportFooterWebsite).innerHTML = sourceTxt.trim();

    } else {
        throw FieldUndefinedException(reportFooterWebsite);
    }

};