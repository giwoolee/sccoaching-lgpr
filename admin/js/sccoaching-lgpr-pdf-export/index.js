/* jshint undef:false, newcap: false, unused:false */

/**
 * Here we are defining the API to generate PDF Reports
 *
 * @method  textAlign()
 *          getLineHeight()
 *
 * @param jsPDF.API
 */
(function(API) {
    API.textAlign = function(txt, options, x, y) {
        options = options || {};
        // Use the options align property to specify desired text alignment
        // Param x will be ignored if desired text alignment is 'center'.
        // Usage of options can easily extend the function to apply different text
        // styles and sizes

        // Get current font size
        var fontSize = this.internal.getFontSize();

        // Get page width
        var pageWidth = this.internal.pageSize.width;

        // Get the actual text's width
        // You multiply the unit width of your string by your font size and divide
        // by the internal scale factor. The division is necessary
        // for the case where you use units other than 'pt' in the constructor
        // of jsPDF.

        var txtWidth = this.getStringUnitWidth(txt) * fontSize / this.internal.scaleFactor;

        if (options.align === "center") {

            // Calculate text's x coordinate
            x = (pageWidth - txtWidth) / 2;

        } else if (options.align === "centerAtX") { // center on X value

            x = x - (txtWidth / 2);

        } else if (options.align === "right") {

            x = x - txtWidth;
        }

        // Draw text at x,y
        this.text(txt, x, y);
    };

    API.textWidth = function(txt) {
        var fontSize = this.internal.getFontSize();
        return this.getStringUnitWidth(txt)*fontSize / this.internal.scaleFactor;
    };

    API.getLineHeight = function(txt) {
        return this.internal.getLineHeight();
    };

})(jsPDF.API);

(function( $ ) {
    'use strict';

    $(function() {
        // preview
        var can_display_preview = true, // if true a preview of the PDF can be displayed in the iFrame
            preview_container = $('#pdf_preview'),
            update_preview_button = $('#flyer_preview_btn'),
            download_button = $('#flyer_download_btn');

        // preview can be displayed?
        if (navigator.msSaveBlob) { // older IE
            update_preview_button.prop('disabled', true);
            can_display_preview = false;
            preview_container.replaceWidth(
                '<div class="no_iframe">' +
                '<div>' +
                "The preview can't be displayed" +
                '</div>' +
                '</div>'
            );
        }

        // utilities
        var hex2rgb = function(hex_string) {
                if (/^#/.test(hex_string)) {
                    hex_string = hex_string.substr(1);
                }
                if (hex_string.length === 3) {
                    hex_string = hex_string.replace(/\w/, function(match) {
                        return String(match) + String(match);
                    });
                }

                return {
                    red: parseInt(hex_string.substr(0, 2), 16),
                    green: parseInt(hex_string.substr(2, 2), 16),
                    blue: parseInt(hex_string.substr(4, 2), 16)
                };
            },

            px2mm = function(pixel) {
                // px to inches
                var inches = pixel / 72;
                return inches * 25.4;
            },

            mm2px = function(millimeters) {
                // mm to inches
                var inches = millimeters / 25.4;
                return inches * 72;
            },

            /* FUNTION TO CALCULATE AND CHECK IMG SIZES */
            imgSizes = function(img_w, img_h, img_mm_w) {
                /*
                 img_w and img_h represent the original image size, in pixel
                 img_mm_w is the desired rendered image size, in millimeters
                 */

                if (img_mm_w > content_width) { // this should be never used...
                    img_mm_w = content_width;
                }

                if (mm2px(img_mm_w) > img_w) {
                    throw 'The `img_mm_w` parameter is too big';
                }

                var img_mm_h = Math.round((px2mm(img_h) * img_mm_w) / px2mm(img_w));

                return {
                    w: img_mm_w,
                    h: img_mm_h,
                    centered_x: (page_width - img_mm_w) / 2
                };
            };


        /* STARTS THE PDF CREATION PROCESS */
        try {

            // BOOTSTRAP
            // http://www.bootply.com/62811
            // Display selected item in dropdown button
            $(".dropdown-menu li a").click(function(){
                $("#flyer-report-author:first-child").html($(this).text()+' <span class="caret"></span>');
            });

            /* REPORT DATE */
            var now = new Date();
            var todayDateFormat = now.getFullYear();
                todayDateFormat += "-" + parseInt(now.getMonth() + 1);
                todayDateFormat += "-" + now.getDate();
            $("#flyer-report-date").datepicker({ dateFormat: 'yy-mm-dd' }).val(todayDateFormat);

            /* CONVERT TABLE HTML TO IMAGE */
            var tableCanvas = {};
            html2canvas($('#flyer-report-table').get(0), {
                onrendered: function(canvas) {
                    tableCanvas = canvas;
                }
            });

            /* UPDATE PREVIEW OPTIONS */
            var update_preview = {
                init: "init",           // Executed when page loads.
                update: "update",       // Executed when update button is pressed.
                download: "download"    // Executed when download button is pressed.
            };

            var pdfReport;

            //!pdf builder
            var createPDF = function(generate_report) {
                /****************************
                 * update_preview:
                 * ==> true: shows pdf online
                 * ==> false: downloads the pdf
                 ****************************/

                // ****************************
                // ******** PDF Output ********
                // ****************************
                if (generate_report == update_preview.init) {

                    var templateRootDir = "../wp-content/plugins/sccoaching-lgpr/admin/partials/template/";
                    preview_container.attr('src', templateRootDir+'template.html');

                    /* Update header banner image when file is uploaded */
                    pdfReport = new PDFReport();
                    pdfReport.init();

                } else if (generate_report == update_preview.update) {

                    pdfReport = new PDFReport();

                    // Update Report Top Section
                    /*pdfReport.setReportTopBanner();  // removed 2017-03-07 */
                    pdfReport.setReportHeaderDescription();

                    // Update Report Legend Section
                    /*pdfReport.setReportLegendBanner(); //removed 2017-03-07 */
                    pdfReport.setReportDate();
                    pdfReport.setReportCoach();

                    // Report Questionnaire Section
                    pdfReport.setReportQuestionnaire();

                    // Report Footer Section
                    pdfReport.setReportFooterCoaches();
                    pdfReport.setReportFooterCities();
                    pdfReport.setReportFooterCountries();
                    pdfReport.setReportFooterLanguages();
                    pdfReport.setReportFooterProcess();
                    pdfReport.setReportFooterEmail();
                    pdfReport.setReportFooterWebsite();

                } else {

                    // Open the LGPR Email PDF page in another page
                    var urlData = '../wp-content/plugins/sccoaching-lgpr/admin/Sccoaching_Lgpr_Email_PDF.php';
                    window.open(urlData, '_blank');

                }
            }; // end createPDF

            // !buttons
            update_preview_button.click(function() {
                createPDF(update_preview.update);
            });

            $('#flyer_download_btn').click(function() {
                createPDF(update_preview.download);
            });

            // Run the initial preview
            createPDF(update_preview.init);

        } catch (e) {
            console.log("Error exporting PDF: " + e);
        }

    });
})( jQuery );
