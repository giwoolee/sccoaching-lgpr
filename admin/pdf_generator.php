<?php

/* mPDF */
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/vendor/mpdf/mpdf/mpdf.php';

/* Initiate PHP buffer */
ob_start();
$pdfURL = 'http://localhost/wordpress/wp-content/plugins/sccoaching-lgpr/admin/partials/template/template.html';
/*$pdfURL = 'http://sccoaching.staging.wpengine.com/wp-content/plugins/sccoaching-lgpr/admin/partials/template/template.html';*/
/* Getting the content from buffer and cleaning the memory */
$pdfHtml = ob_get_clean();

/* Convert content to utf-8 */
$pdfHtml = utf8_encode($pdfHtml);

$pdfHtml .= file_get_contents($pdfURL);

$mpdf = new mPDF();

$mpdf->useAdobeCJK = true;
$mpdf->autoScriptToLang = true;
$mpdf->autoLangToFont = true;
$mpdf->allow_charset_conversion = true;
$mpdf->charset_in = "UTF-8";
$mpdf->CSSselectMedia='mpdf';
$mpdf->SetBasePath($pdfURL);
$mpdf->WriteHTML($pdfHtml);
$mpdf->SetHeader("Leadership Growth Progress Review | Marshall Goldsmith | {PAGENO}");
$mpdf->SetFooter("Leadership Growth Progress Review");


$mpdf->Output('lgpr-pdf', 'I');
exit();

/* TCPDF */
/*require_once('/TCPDF/tcpdf.php');
$htmlBase = 'http://sccoaching.staging.wpengine.com/wp-content/plugins/sccoaching-lgpr/admin/partials/template/';
$htmlImg = "Web_Banner_950x174.jpg";
$htmlFile = 'template.html';

$buffer = "<<<EOF";
$buffer .= file_get_contents($htmlBase . $htmlFile);
$buffer .= "EOF;";

$tcpdf = new TCPDF('P', 'mm', 'A4', true, false, true);
$tcpdf->AddPage('P','A4');
$img = file_get_contents($htmlBase . $htmlImg);
$tcpdf->Image('Web_Banner_950x174.jpg' . $img, 950, 174, '', '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
$tcpdf->writeHTML($buffer, true, false, true, false, '');
$tcpdf->Output('output.pdf', 'F');*/

/* HTML2PDF.IT */
/*require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');
$htmlFile = 'http://sccoaching.staging.wpengine.com/wp-content/plugins/sccoaching-lgpr/admin/partials/template/template.html';
$buffer = file_get_contents($htmlFile);

try
{
    $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(0, 0, 0, 0));
    $html2pdf->setDefaultFont('stsongstdlight');
    $pdfOutput = $html2pdf->writeHTML($buffer);
    $html2pdf->Output('created/'.$lastname[1].'_html2pdf.pdf', 'F');

    // header('Content-Type: application/pdf');
    // header('Cache-Control: no-cache');
    // header('Content-Length: ' . strlen($result));
    // header('Content-Disposition: attachment; filename=' . 'html2pdf.pdf' );

    // echo($result);

} catch(HTML2PDF_exception $e) {
    echo $e;
}*/

/* HTML2PDF ROCKET */
/*$apikey = '078d8e32-7f7f-4caa-8666-9179fc74c693';
$value = 'http://sccoaching.staging.wpengine.com/wp-content/plugins/sccoaching-lgpr/admin/partials/template/template.html';

// Make sure your margins are large enough to allow the header to fit
$postdata = http_build_query(
    array(
        'apikey' => $apikey,
        'value' => $value,
        'UsePrintStylesheet' => true,
        'ViewPort' => ''
        'PageWidth' => '210mm',
        'PageHeight' => '297mm',
        'HeaderUrl' => 'http://www.html2pdfrocket.com/examples/basicheader.html'
    )
);

$opts = array('http' =>
    array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => $postdata
    )
);

$context  = stream_context_create($opts);

// $result = file_get_contents("http://api.html2pdfrocket.com/pdf?apikey=" . urlencode($apikey) . "&value=" . urlencode($value));
$result = file_get_contents("http://api.html2pdfrocket.com/pdf", false, $context);
header('Content-Description: File Transfer');
header('Content-Type: application/pdf');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . strlen($result));
header('Content-Disposition: attachment; filename=' . 'html2pdf_rocket.pdf' );
echo($result);*/

/*FREEHTMLTOPDF*/
/*$url = 'http://freehtmltopdf.com';
$baseurl = __DIR__ . '\partials\template\\';
$pdfFile = __DIR__ . '\partials\template\template.html';
$pdfurl = 'http://sccoaching.staging.wpengine.com/wp-content/plugins/sccoaching-lgpr/admin/partials/template/template.html';

if (file_exists($pdfFile)) {
    ob_start();
    $pdfcontent = file_get_contents($pdfFile);
    echo $pdfcontent;
    $htmlcontent = ob_get_contents();
    ob_end_clean();

    if ($pdfurl) {
        $data = array(  'convert' => $pdfurl,
                        'enablejs' => 1,
                        'size' => 'A4',
                        'orientation' => 'portrait');
    } else {
        $data = array(  'convert' => '',
                        'html' => $htmlcontent,
                        'baseurl' => $baseurl);
    }

    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data),
        ),
    );

    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);

    header('Content-type: application/pdf');
    header('Content-Disposition: attachment; filename="webpage.pdf"');
    echo($result);
} else {
    var_dump($pdfFile);
    var_dump(file_exists($pdfFile));
}*/

/* PDFCROWD */
/*try
{
    $pdfFile = __DIR__ . '\partials\template\template.html';

    if (file_exists($pdfFile)) {

        $client = new Pdfcrowd("ggl89", "a5d9d87e944daffd19175fecc31ca894");

        $pdf = $client->convertFile($pdfFile);

        $client->enableBackgrounds(True);
        $client->enableJavascript(True);
        $client->usePrintMedia(True);
        $client->setPageMargins(0,0,0,0);
        $client->setAuthor('Marshall Goldsmith - Stakeholder Centered Coaching');
        $client->setPageWidth('210mm');
        $client->setPageHeight('297mm');

        header("Content-Type: application/pdf");
        header("Cache-Control: no-cache");
        header("Accept-Ranges: none");
        header("Content-Disposition: inline; filename=\"lgprreport.pdf\"");

        echo $pdf;
    }
}
catch(PdfcrowdException $why)
{
    echo "Pdfcrowd Error: " . $why;
}*/

?>