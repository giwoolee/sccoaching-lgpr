(function($) {
    'use strict'

    $(function() {

        var currentDate = new Date();
        var page = $('.page')[0];
        var contentPages = $('.page');

        /*for (var page in pages) {
            var pageNumberDiv = "<div class=\"page-number\">Page " + page + " </div>";
        };*/

        for (var i = 0; i < contentPages.length; i++) {
            var page = contentPages[i];
            var pageNumber = i + 1; // This is done because there's no Page 0(Zero)

            // Appending page-number element to page class
            var newDiv = document.createElement('div');
            newDiv.setAttribute('class', 'page-number');
            newDiv.innerHTML = "Page " + pageNumber + " of " + contentPages.length;
            page.appendChild(newDiv);
        }

        // Appending printing element
        var printingDiv = document.createElement('button');
        printingDiv.setAttribute('class', 'page-printing');
        printingDiv.innerHTML = "<i class=\"glyphicon glyphicon-print\"></i> Print LGPR report";
        page.appendChild(printingDiv);

        // Appending printing to PDF element
        /*var printingToPdfDiv = document.createElement('button');
        printingToPdfDiv.setAttribute('class', 'page-printing-to-pdf');*/
        var printingToPdfDiv = document.createElement('a');
        printingToPdfDiv.setAttribute('class', 'btn btn-default page-printing-to-pdf');
        printingToPdfDiv.innerHTML = "<i class=\"glyphicon glyphicon-file\"></i> Download PDF";
        page.appendChild(printingToPdfDiv);

        // Button to print page
        $('.page-printing').click(function(e) {
            e.preventDefault();
            window.print();
        });

        var params = {
            url: 'http://sccoaching.staging.wpengine.com/wp-content/plugins/sccoaching-lgpr/admin/partials/template/template.html',
            format: 'A4',
            orientation: 'portrait',
            margin: '0cm',
            download: true,
            filename: 'lgpr_report'
        };

        var endpoint = 'http://www.html2pdf.it/';
        var link = endpoint + "?" +
            "url=" + params.url +
            "&format=" + params.format +
            "&orientation=" + params.orientation +
            "&margin=" + params.margin +
            "&download=" + params.download +
            "&filename=" + params.filename;

        $(".page-printing-to-pdf").attr('href', link);

        // Setting the production date for the document
        var productionDate = $('.lgpr-report-date');
        var month = currentDate.getMonth() + 1;
        var formatDate = currentDate.getFullYear() + "-" + month + "-" + currentDate.getDate();
        $.each(productionDate, function(index, value) {
            /*value.html("Production Date: " + currentDate.toLocaleDateString());*/
            value.innerHTML = "Production Date: " + formatDate;
        });

    });

})(jQuery);