<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Sccoaching_Lgpr
 * @subpackage Sccoaching_Lgpr/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="wrap">
    <h1>LGPR (Leadership Growth Progress Report)</h1>
    <h3>Edit Survey</h3>
    <form action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" id="edit_survey_form">
        <input type="hidden" name="action" value="sccoaching_lgpr_edit_survey_form" />
        <p>
            <label for="name">
                <small>Name (required)</small>
            </label>
            <input type="text" name="name" value="<?php echo $_GET['name'] ?>" size="22" tabindex="1" autofocus />
        </p>
        <p>
            <label for="country">
                <small>Country (required)</small>
            </label>
            <input type="text" name="country" value="<?php echo $_GET['country'] ?>" maxlength="20" size="22" tabindex="1" />
            <small>Max. 20 characters</small>
        </p>
        <p>
            <div class="alert" id="response_survey"></div>
        </p>
        <p>
            <input type="submit" tabindex="5" value="Edit Survey" />
        </p>
    </form>
</div>
