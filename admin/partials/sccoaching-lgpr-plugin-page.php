<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Sccoaching_Lgpr
 * @subpackage Sccoaching_Lgpr/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="wrap">
    <h1>LGPR (Leadership Growth Progress Report)</h1>
    <h3>Admin Page</h3>
    <div><?php echo do_shortcode( '[sccoaching_lgpr_survey]' ); ?></div>
</div>
