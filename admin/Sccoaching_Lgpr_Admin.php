<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * Define the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript
 *
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Admin
{

    /**
     * The user controller for this plugin.
     *
     * @since   1.0.0
     * @var Sccoaching_Lgpr_Report_Template_Controller
     * @access private
     */
    public $report_template_controller;

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

        $this->load_dependencies();
        $this->report_template_controller = new Sccoaching_Lgpr_Report_Template_Controller();

    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/sccoaching-lgpr-admin.css', array(), $this->version, 'all' );

        /* Register bootstrap style CDN */
        wp_register_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
        wp_enqueue_style('bootstrap');

        wp_register_style('jquery-ui-datepicker-style' , '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css');
        wp_enqueue_style('jquery-ui-datepicker-style');

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/sccoaching-lgpr-survey.js', array( 'jquery' ), $this->version, false );
        wp_enqueue_script( $this->plugin_name . "-report-template", plugin_dir_url( __FILE__ ) . 'js/sccoaching-lgpr-report-template.js', array( 'jquery' ), $this->version, false );

        /* Vendor CSS & Script */
        $sccoaching_pdf_export_fields_name = 'sccoaching-lgpr-pdf-export-fields';
        $sccoaching_pdf_export_name = 'sccoaching-lgpr-pdf-export';
        $vendor_pdfgen_fonts_name = 'pdfmake-fonts';
        $vendor_jspdf_name = 'jspdf';
        $vendor_jspdf_autotable_plugin_name = 'jspdf-autotable-plugin';
        $vendor_bootstrap = 'bootstrap';

        /* Register & enqueue all PDF Export JS' for site */
        wp_register_script( $vendor_jspdf_name, plugin_dir_url( __FILE__ ) . 'js/vendor/jspdf.min.js' );
        wp_register_script( $vendor_jspdf_autotable_plugin_name, plugin_dir_url( __FILE__ ) . 'js/vendor/jspdf.plugin.autotable.js' );
        wp_register_script( $vendor_pdfgen_fonts_name, plugin_dir_url( __FILE__ ) . 'js/vendor/vfs_fonts.js' );
        wp_register_script( $sccoaching_pdf_export_fields_name, plugin_dir_url( __FILE__ ) . 'js/sccoaching-lgpr-pdf-export/PDFReport.js' );
        wp_register_script( $sccoaching_pdf_export_name, plugin_dir_url( __FILE__ ) . 'js/sccoaching-lgpr-pdf-export/index.js' );
        wp_register_script( $vendor_bootstrap, 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');

        wp_enqueue_script( $vendor_jspdf_name );
        wp_enqueue_script( $vendor_jspdf_autotable_plugin_name );
        wp_enqueue_script( $vendor_pdfgen_fonts_name );
        wp_enqueue_script( $sccoaching_pdf_export_fields_name );
        wp_enqueue_script( $sccoaching_pdf_export_name );
        wp_enqueue_script( $vendor_bootstrap );

        /* jQuery UI */
        wp_enqueue_script('jquery-ui-datepicker');

        /* Register the localize script for the admin area */
        wp_localize_script( $this->plugin_name, 'AdminAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

    }

    public function define_rest_route() {
        register_rest_route( 'sccoaching-lgpr/v1', '/author/(?P<id>\d+)', array(
            'methods' => WP_REST_Server::READABLE,
            'callback' => 'my_awesome_func',
        ) );
    }

    /**
    * Grab latest post title by an author!
    *
    * @param array $data Options for the function.
    * @return string|null Post title for the latest,  * or null if none.
    */
    public function my_awesome_func( $data ) {
        $posts = get_posts( array(
            'author' => $data['id'],
        ) );

        if ( empty( $posts ) ) {
            return new WP_Error( 'awesome_no_author', 'Invalid author', array( 'status' => 404 ) );
        }

        return $posts[0]->post_title;
    }

    /**
     * Add the admin menu link.
     *
     * @since    1.0.0
     */
    public function add_menu_item() {

        add_menu_page (
            __( 'SCCoaching LGPR', 'textdomain'),
            __( 'MGSCC LGPR Admin Page' ),
            'manage_options',
            __FILE__,
            array($this, 'sccoaching_lgpr_render_plugin_page'),
            '',
            6
        );

    }

    /**
     * Add the admin submenu link.
     *
     * @since   1.0.0
     */
    public function add_submenu_item() {

        add_submenu_page(
            __FILE__,
            'Add New Survey',
            'Add New Survey',
            'manage_options',
            __FILE__.'/add',
            array($this, 'sccoaching_lgpr_render_add_page')
        );

        add_submenu_page(
            __FILE__,
            'Report Template',
            'Report Template',
            'manage_options',
            __FILE__.'/report_template',
            array($this, 'sccoaching_lgpr_render_report_templating_page')
        );

        add_submenu_page(
            __FILE__,
            'About',
            'About',
            'manage_options',
            __FILE__.'/about',
            array($this, 'sccoaching_lgpr_render_about_page')
        );

    }

    /**
     * Render LGPR plugin page
     *
     * @since   1.0.0
     */
    public function sccoaching_lgpr_render_plugin_page() {
        // check user capabilities
        if (!current_user_can('manage_options')) {
            return;
        }

        require_once 'partials/sccoaching-lgpr-plugin-page.php';
    }

    /**
     * Render LGPR plugin submenu add page
     */
    public function sccoaching_lgpr_render_add_page() {
        // check user capabilities
        if (!current_user_can('manage_options')) {
            return;
        }

        require_once 'partials/sccoaching-lgpr-plugin-add-page.php';
    }

    /**
     * Render LGPR plugin submenu for Email Templating
     */
    public function sccoaching_lgpr_render_report_templating_page() {
        // check user capabilities
        if (!current_user_can('manage_options')) {
            return;
        }

        require_once 'partials/sccoaching-lgpr-plugin-report-templating-page.php';
    }

    /**
     * Render LGPR plugin submenu edit page
     */
    public function sccoaching_lgpr_render_edit_page() {
        // check user capabilities
        if (!current_user_can('manage_options')) {
            return;
        }

        require_once 'partials/sccoaching-lgpr-plugin-edit-page.php';
    }

    /**
     * Render LGPR's plugin submenu about page
     */
    public function sccoaching_lgpr_render_about_page() {
        // check user capabilities
        if (!current_user_can('manage_options')) {
            return;
        }

        require_once 'partials/sccoaching-lgpr-plugin-about-page.php';
    }

    /**
     * Load the dependencies for this class
     */
    public function load_dependencies() {

        /**
         * The class responsible for controlling the routes to views
         */
         require_once plugin_dir_path( dirname( __FILE__ ) ) . 'controllers/Sccoaching_Lgpr_Report_Template_Controller.php';
    }
}