<?php

// Get cURL resource
$curl = curl_init();

// Set some options
curl_setopt_array($curl, array(
	CURLOPT_HTTPHEADER => array(
		'Authentication: Token <your token>', // Don't forget to change the token!!!
	),
	CURLOPT_RETURNTRANSFER => 1,
	CURLOPT_URL => 'https://htmlpdfapi.com/api/v1/pdf',
	CURLOPT_POST => 1,

	// URL
	CURLOPT_POSTFIELDS => array(
		'url' => 'https://htmlpdfapi.com/examples/example.html',
	),
));

// Send the request & save response to $resp
$resp = curl_exec($curl);

//Save file
file_put_contents('result.pdf', $resp);

// Close request to clear up some resources
curl_close($curl);
