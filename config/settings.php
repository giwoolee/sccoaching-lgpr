<?php
/**
 * Sccoaching Lgpr Settings File
 *
 * This class defines all code necessary to run during this plugin's actions.
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */

return [
    'profile' => [
        'point'     => [
            'standard_section' => 1,
            'years_experience' => 5,
            'certification_level' => [
                '1' => 30,
                '2' => 20,
                '3' => 10
            ]
        ],
        'location'  => [
            'count' => 5
        ],
        'years_experience' => [
            '< 2',
            '2-5',
            '6-10',
            '> 10'
        ],
        'view'      => [
            'max'   => 20
        ]
    ],
    'routes'   => [
        'get_profile'       => '/coach/profile',
        'get_edit_profile'  => '/coach/edit/%s',
        'get_coach'         => '/coach/%s',
    ]
];