# SCCOACHING LGPR (Leadership Growth Performance Report) #

- @link              http://sccoaching.com
- @since             1.0.0
- @package           Sccoaching_LGPR

- @wordpress-plugin
- Plugin Name:       SCCoaching LGPR
- Plugin URI:        http://sccoaching.com/lgpr/
- Description:       MGSCC - Leadership Growth Progress Report
- Version:           1.0.0
- Author:            Gustavo Lee
- Author URI:        https://www.linkedin.com/profile/preview?locale=en_US&trk=prof-0-sb-preview-primary-button
- License:           GPL-2.0+
- License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
- Text Domain:       sccoaching-lgpr
- Domain Path:       /languages