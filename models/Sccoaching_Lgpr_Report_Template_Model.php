<?php

require_once 'foundation/Sccoaching_Lgpr_Base_Model.php';

/**
 * Report Template Model
 *
 * This class defines all code necessary to run during this plugin's actions.
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Report_Template_Model extends Sccoaching_Lgpr_Base_Model
{
    /**
     * Configuration reference.
     *
     * @var object
     */
    private $config;

    /**
     * Constructor.
     */
    public function __construct() {

        $this->load_dependencies();

        parent::__construct();
        $this->set_table( 'sccoaching_lgpr_report_template' );

        $this->config = new Sccoaching_Lgpr_Config();

    }

    /**
     * Save the model.
     */
    public function save() {

        $result = $this->create();

        echo "Rows inserted: " . json_encode($result);

    }

    /*
     * Get the last created report template
     *
     * @return mixed
     */
    public function report_template_get_latest() {

        $item = $this->order_by('created_at')
                ->limit(1)
                ->row();

        return $item;
    }

    /**
     * Load the dependencies for this class.
     */
    private function load_dependencies() {

        /**
         * The class responsible for configuration settings.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Sccoaching_Lgpr_Config.php';

    }

}