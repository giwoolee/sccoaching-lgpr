<?php

/**
 * Database Result
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/controllers
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Db_Result
{
    /**
     * @since 1.0.0
     * @access public
     * @var int
     */
    private $count = 0;

    /**
     * @since 1.0.0
     * @access public
     * @var array
     */
    private $objects = [];

    /**
     * Get the count.
     *
     * @return int
     */
    public function get_count() {
        return $this->count;
    }

    /**
     * Get the objects.
     *
     * @return array
     */
    public function get_objects() {
        return $this->objects;
    }


    /**
     * Set the count.
     *
     * @param int $count
     * @return $this
     */
    public function set_count( $count = 0 ) {

        $this->count = $count;
        return $this;

    }

    /**
     * Set the objects.
     *
     * @param array $objects
     * @return $this
     */
    public function set_objects( $objects = [] ) {

        $this->objects = $objects;
        return $this;

    }
}