<?php

require_once 'Sccoaching_Lgpr_Query_Trait.php';

/**
 * Search Trait
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/controllers
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
trait Sccoaching_Lgpr_Search_Trait
{
    use Sccoaching_Lgpr_Query_Trait;

    /**
     * @since 1.0.0
     * @access private
     * @var string
     */
    private $select = '*';

    /**
     * @since 1.0.0
     * @access private
     * @var string
     */
    private $count = '';

    /**
     * @since 1.0.0
     * @access private
     * @var string
     */
    private $distinct = '';

    /**
     * @since 1.0.0
     * @access private
     * @var string
     */
    private $like = '';

    /**
     * @since 1.0.0
     * @access private
     * @var string
     */
    private $where = '';

    /**
     * @since 1.0.0
     * @access private
     * @var string
     */
    private $join = '';

    /**
     * @since 1.0.0
     * @access private
     * @var string
     */
    private $limit = '';

    /**
     * @since 1.0.0
     * @access private
     * @var string
     */
    private $offset = '';

    /**
     * @since 1.0.0
     * @access private
     * @var string
     */
    private $order_by = '';

    /**
     * Prepare the select.
     *
     * @param mixed $fields
     * @return $this
     */
    public function select( $fields ) {

        $this->select = $this->select_query( $fields );
        return $this;

    }

    /**
     * Prepare as distinct.
     *
     * @return $this
     */
    public function distinct() {

        $this->distinct = $this->distinct_query();
        return $this;

    }

    /**
     * Prepare count.
     *
     * @param bool|true $set
     * @return $this
     */
    public function count( $set = true ) {

        $this->count = $set ? $this->count_query() : '';
        return $this;

    }

    /**
     * Prepare the like.
     *
     * @param string $field
     * @param string $value
     * @return $this
     */
    public function like( $field = '', $value = '' ) {

        if (strlen( $this->like ) > 0) {
            $this->like .= ' ' .$this->or_like_query( $field, $value );
        } else {
            $this->like = $this->like_query( $field, $value );
        }
        return $this;

    }

    /**
     * Prepare the where.
     *
     * @param string $field
     * @param string $value
     * @return $this
     */
    public function where( $field = '', $value = '' ) {

        if (strlen( $this->where ) > 0) {
            $this->where .= ' ' . $this->and_where_query( $field, $value );
        } else {
            $this->where = $this->where_query( $field, $value );
        }
        return $this;

    }

    /**
     * Prepare the where in.
     *
     * @param string $field
     * @param array $list
     * @return $this
     */
    public function where_in( $field = '', $list = [] ) {

        if (strlen( $this->where ) > 0) {
            $this->where .= "\n" . $this->and_where_in_query( $field, $list );
        } else {
            $this->where = $this->where_in_query( $field, $list );
        }
        return $this;

    }

    /**
     * Prepare the join.
     *
     * @param string $left_table
     * @param string $left_field
     * @param string $right_table
     * @param string $right_field
     * @param string $type
     * @return $this
     */
    public function join( $left_table = '', $left_field = '', $right_table = '', $right_field = '', $type = '' ) {

        $this->join .= ' ' . $this->join_query( $left_table, $left_field, $right_table, $right_field, strtoupper( $type ) );
        return $this;

    }

    /**
     * Prepare the limit.
     *
     * @param int $number
     * @return $this
     */
    public function limit( $number = 0 ) {

        $this->limit = $this->limit_query( $number );
        return $this;

    }

    /**
     * Prepare the offset.
     *
     * @param int $number
     * @return $this
     */
    public function offset( $number = 0 ) {

        $this->offset = $this->offset_query( $number );
        return $this;

    }

    /**
     * Prepare the order by.
     *
     * @param string $order_by
     * @param bool|true $desc
     * @return $this
     */
    public function order_by( $order_by = '', $desc = true ) {

        $this->order_by = $this->order_by_query( $order_by, $desc );
        return $this;

    }

    /**
     * Get multiple rows.
     *
     * @param string $output_type
     * @return mixed
     */
    public function all( $output_type = OBJECT ) {
        return $this->wpdb->get_results( $this->build_sql(), $output_type );
    }

    /**
     * Gets a single row.
     *
     * @param string $output_type
     * @return mixed
     */
    public function row( $output_type = OBJECT ) {
        return $this->wpdb->get_row( $this->build_sql(), $output_type );
    }

    /**
     * Gets just a variable.
     *
     * @return mixed
     */
    public function variable() {
        return $this->wpdb->get_var( $this->build_sql() );
    }

    /**
     * Builds the sql string.
     *
     * @return string
     */
    private function build_sql() {

        $from_query = $this->from_query();
        $matched_like = $this->match_like_to_where( $this->like, $this->where );
        $select = $this->match_select_to_criteria( $this->select, $this->count, $this->distinct );

        return $sql = "
            $select
            $from_query $this->table
            $this->join
            $this->where
            $matched_like
            $this->order_by
            $this->limit
            $this->offset
            ";

    }
}