<?php

require_once 'Sccoaching_Lgpr_Search_Trait.php';

/**
 * Base Model
 *
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
abstract class Sccoaching_Lgpr_Base_Model
{
    use Sccoaching_Lgpr_Search_Trait;

    /**
     * @since 1.0.0
     * @access public
     * @var array
     */
    public $stored;

    /**
     * @since 1.0.0
     * @access protected
     * @var bool
     */
    public $exists;

    /**
     * @since 1.0.0
     * @access protected
     * @var string
     */
    protected $table;

    /**
     * @since 1.0.0
     * @access protected
     * @var object
     */
    protected $wpdb;

    /**
     * @since 1.0.0
     * @access protected
     * @var array
     */
    protected $data;

    /**
     * @since 1.0.0
     * @access protected
     * @var array
     */
    protected $format;

    /**
     * Save the model.
     *
     * @return mixed
     */
    abstract public function save();

    /**
     * Constructor.
     */
    public function __construct() {

        global $wpdb;
        $this->wpdb = $wpdb;
        $this->data = [];
        $this->format = [];
        $this->stored = new stdClass();

    }

    /**
     * Try to get stored data with some magic.
     *
     * @param $name
     * @return string
     */
    public function __get( $name ) {

        if (!is_null( $this->stored ) && array_key_exists( $name, $this->stored )) {
            return $this->stored->{$name};
        }

        return '';

    }

    /**
     * Set a field.
     *
     * @param string $field
     * @param $value
     * @return $this
     */
    public function set( $field = '', $value ) {

        $this->data[$field] = $value;
        $this->format[$field] = is_string( $value ) ? '%s' : '%d';

        return $this;

    }

    /**
     * Properly sets the table name.
     *
     * @param string $name
     */
    protected function set_table( $name = '' ) {
        $this->table = $this->wpdb->prefix . $name;
    }

    /**
     * Update a model.
     *
     * @param array $where
     * @return mixed
     */
    protected function update( $where = [] ) {
        return $this->wpdb->update( $this->table, $this->data, $where, $this->get_format() );
    }

    /**
     * Create a new model.
     *
     * @return int|false - The number of rows updated, or false on error.
     */
    protected function create() {

        $data = $this->data;
        $format = $this->get_format();

        $result = $this->wpdb->insert($this->table, $data, $format);

        return $result;

    }

    /**
     * Delete a model.
     *
     * @param array $where
     * Modify By Boris 2016/02/19
     *
     * @return int|false - The number of rows updated, or false on error.
     */
    protected function delete( $where = [] ) {
        $result = $this->wpdb->delete( $this->table, $where );
        return $result;
    }

    /**
     * We want to get the format in the same order that the data is.
     *
     * @return array
     */
    private function get_format() {

        $d = [];
        foreach ($this->data as $k => $v) $d[] = $this->format[$k];

        return $d;

    }
}