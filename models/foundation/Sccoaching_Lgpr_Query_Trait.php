<?php

/**
 * Search Trait
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/controllers
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
trait Sccoaching_Lgpr_Query_Trait
{
    /**
     * Select query.
     *
     * @param mixed $fields
     * @return string
     */
    private function select_query( $fields ) {

        if (sizeof( $fields ) > 0) {
            if (is_array( $fields )) {
                return implode( ',', $fields );
            } else {
                return $fields;
            }
        }

        return '*';

    }

    /**
     * From query.
     *
     * @return string
     */
    private function from_query() {
        return "FROM";
    }

    /**
     * Distinct query.
     *
     * @return string
     */
    private function distinct_query() {
        return 'DISTINCT';
    }

    /**
     * Count query.
     *
     * @return string
     */
    private function count_query() {
        return 'COUNT';
    }

    /**
     * Where query.
     *
     * @param string $field
     * @param string $value
     * @return string
     */
    private function where_query( $field = '', $value = '' ) {

        if ($field && $value) {
            $format = is_string( $value ) ? '%s' : '%d';
            return $this->wpdb->prepare( "WHERE $field = $format", $value );
        }

        return '';

    }

    /**
     * And where query.
     *
     * @param string $field
     * @param string $value
     * @return mixed|string
     */
    private function and_where_query( $field = '', $value = '' ) {

        if ($field && $value) {
            return str_replace( 'WHERE', 'AND', $this->where_query( $field, $value ) );
        }

        return '';
    }

    /**
     * Where in query.
     *
     * @param string $field
     * @param array $list
     * @return string
     */
    private function where_in_query( $field = '', $list = [] ) {

        if ($field && $list) {
            $prep_list = array_map(function($o) { return "\"$o\""; }, $list);
            $value = implode( ',', $prep_list );
            return "WHERE $field in ($value)";
        }

        return '';

    }

    /**
     * And where in query.
     *
     * @param string $field
     * @param string $value
     * @return mixed|string
     */
    private function and_where_in_query( $field = '', $value = '' ) {

        if ($field && $value) {
            return str_replace( 'WHERE', 'AND', $this->where_in_query( $field, $value ) );
        }

        return '';
    }

    /**
     * Like query.
     *
     * @param string $field
     * @param string $value
     * @return string
     */
    private function like_query( $field = '', $value = '' ) {

        if ($field && $value) {
            $format = is_string( $value ) ? '%s' : '%d';
            $safe_value = $this->wpdb->prepare( '%%' . $format . '%%', $value );
            return "$field LIKE '$safe_value'";
        }

        return '';

    }

    /**
     * Or like query.
     *
     * @param string $field
     * @param string $value
     * @return mixed|string
     */
    private function or_like_query( $field = '', $value = '' ) {

        if ($field && $value) {
            return 'OR ' . $this->like_query( $field, $value );
        }

        return '';
    }

    /**
     * Join query.
     *
     * @param string $left_table
     * @param string $left_field
     * @param string $right_table
     * @param string $right_field
     * @param string $type
     * @return string
     */
    private function join_query( $left_table = '', $left_field = '', $right_table = '', $right_field = '', $type = '' ) {

        $prefix = $this->wpdb->prefix;
        if ($left_table && $left_field && $right_table && $right_field) {
            return "
                $type JOIN {$prefix}{$right_table}
                ON {$prefix}{$right_table}.{$right_field} = {$prefix}{$left_table}.{$left_field}
            ";
        }

        return '';

    }

    /**
     * Limit query.
     *
     * @param int $limit
     * @return string
     */
    private function limit_query( $limit = 0 ) {

        if (!is_null( $limit )) {
            return "LIMIT $limit";
        }

        return '';

    }

    /**
     * Offset Query.
     *
     * @param int $offset
     * @return string
     */
    private function offset_query( $offset = 0 ) {

        if (!empty($offset)) {
            return "OFFSET $offset";
        }

        return '';

    }

    /**
     * Order by query.
     *
     * @param string $order_by
     * @param bool|true $desc
     * @return string
     */
    private function order_by_query( $order_by = '', $desc = true ) {

        if ($order_by) {
            $direction = $desc ? 'DESC' : 'ASC';
            return "ORDER BY $order_by $direction";
        }

        return '';
    }

    /**
     * Need to match the select with the count ahd distinct queries.
     *
     * @param string $select
     * @param string $count
     * @param string $distinct
     * @return string
     */
    private function match_select_to_criteria( $select = '', $count = '', $distinct = '' ) {

        if (strlen( $count ) > 0) {
            return "SELECT {$count}({$distinct} {$select})";
        } else {
            return "SELECT $distinct $select";
        }

    }

    /**
     * We need to match the like to the where, or else there are two wheres.
     *
     * @param string $like
     * @param string $where
     * @return mixed|string
     */
    private function match_like_to_where( $like = '', $where = '' ) {

        $new_like = '';

        if (strlen( $like ) > 0) {
            if (strlen( $where ) > 0) {
                $new_like = sprintf( 'AND (%s)', $like );
            } else {
                $new_like = sprintf( 'WHERE (%s)', $like );
            }
        }

        return $new_like;

    }
}