<?php

require_once 'foundation/Sccoaching_Lgpr_Base_Model.php';

/**
 * Survey Model
 *
 * This class defines all code necessary to run during this plugin's actions.
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Survey_Model extends Sccoaching_Lgpr_Base_Model
{
    /**
     * Configuration reference.
     *
     * @var object
     */
    private $config;

    /**
     * Constructor.
     */
    public function __construct() {

        $this->load_dependencies();

        parent::__construct();
        $this->set_table( 'sccoaching_lgpr_survey' );
        $this->stored = new stdClass();

        $this->config = new Sccoaching_Lgpr_Config();

    }

    /**
     * Save the model.
     */
    public function save() {

        $result = $this->create();

        echo json_encode($result);

    }

    /**
     * Remove the item
     *
     */
    public function delete_by_id($id) {

        $where = array('id' => $id);
        $result = $this->delete($where);

        echo json_encode($result);

    }

    /**
     * Get a list of surveys.
     * 
     * @return mixed
     */
    public function survey_list_all() {
        
        $items = $this->order_by('id')->all();
        return $items;
        
    }

    /**
     * Load the dependencies for this class.
     */
    private function load_dependencies() {

        /**
         * The class responsible for configuration settings.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Sccoaching_Lgpr_Config.php';


    }

}