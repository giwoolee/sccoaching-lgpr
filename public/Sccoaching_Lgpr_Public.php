<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Public
{
    /**
     * The user controller for this plugin.
     *
     * @since   1.0.0
     * @var Sccoaching_Lgpr_User_Controller
     * @access private
     */
    public $user_controller;

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since      1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

        $this->load_dependencies();
        $this->survey_controller = new Sccoaching_Lgpr_Survey_Controller();

    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/sccoaching-lgpr-public.css', array(), $this->version, 'all' );

    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/sccoaching-lgpr-public.js', array( 'jquery' ), $this->version, false );

        /* Vendor css & script */
        $vendor_pdfgen_name = 'pdfmake';
        $vendor_pdfgen_fonts_name = 'pdfmake-fonts';

        /* Register & enqueue all PDF Export JS' for site */
        wp_register_script( $vendor_pdfgen_name, plugin_dir_url( __FILE__ ) . 'js/vendor/pdfmake.min.js' );
        wp_register_script( $vendor_pdfgen_fonts_name, plugin_dir_url( __FILE__ ) . 'js/vendor/vfs_fonts.js' );
        wp_enqueue_script( $vendor_pdfgen_name );
        wp_enqueue_script( $vendor_pdfgen_fonts_name );

        /* Register the localize script for the admin area */
        wp_localize_script( $this->plugin_name, 'AdminAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

    }

    /**
     * Load the dependencies for this class.
     */
    private function load_dependencies() {

        /**
         * The class responsible for controlling the routes to views.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'controllers/Sccoaching_Lgpr_Survey_Controller.php';

    }
}