(function( $ ) {
    'use strict';

    /**
     * Selectors for DOM elements.
     *
     * @type {{searchForm: string, results: string}}
     */
    var selectors = {
        addSurveyForm: '#addSurveyForm',
    }, attrSelectors = {
        action: 'action'
    };

    $(function() {

        /**
         * Coach search submit.
         *
         * @param e
         * @returns {boolean}
         */
        $(selectors.addSurveyForm).submit(function(e) {
            e.preventDefault();
            var form = jQuery(this);
            var _data = { 'action' : 'sccoaching_lgpr_survey' };

            $.post(
                form.attr(attrSelectors.action),
                {
                    action: 'sccoaching_lgpr_survey'
                },
                function(response) {
                   alert('The server responded: ' + response);
                }
            );

            return false;
        });

        $(".engagement-export-preview").on("click", function() {
            // Testing if works
            // alert("PDF GENERATOR CLICKED - " + $(this).data("id"));
            console.log("PDF GENERATOR CLICKED - " + $(this).data("id"));
            
            // Printing for PDF from JS source code
            // var docDefinition = {
            //     content: 'This is an sample PDF printed with pdfMake for ID: ' + $(this).data("id")
            // };

            // Retrieve data from the row
            var id = $(this).data("id"),
                name = $(this).data("name"),
                country = $(this).data("country");

            // Get current date info
            var currentDate = new Date(Date.now());
            var year = currentDate.getFullYear();
            var month = ('0' + (currentDate.getMonth() + 1)).slice(-2);
            var day = ('0' + currentDate.getDate()).slice(-2);
            var formattedDate = year + month + day;

            // Defining the PDF document to be generated
            var docDefinition = {
                header: {
                    text: formattedDate,
                    alignment: 'right'
                },
                content: [
                    {
                        table: {
                            // headers are automatically repeated if the table spans over multiple pages
                            // you can declare how many rows should be treated as headers
                            headerRows: 1,
                            widths: [ 10, '*', 100, 100 ],
                            body: [
                                [ '#', 'Name', 'Country', 'Date' ],
                                [ { text: id, bold: true }, name, country, currentDate.toDateString() ]
                            ]
                        }
                    }
                ]
            }

            // Open the PDF in a new window
            pdfMake.createPdf(docDefinition).open();

            // Print the PDF
            // pdfMake.createPdf(docDefinition).print();

            // Download the PDF
            // pdfMake.createPdf(docDefinition).download('LGPR-' + formattedDate );
        });

    });

})( jQuery );
