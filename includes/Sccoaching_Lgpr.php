<?php

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin
 *
 * Time: 7:31 PM
 * Date: 2017.02.10
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr
{
    /**
     * The loader that's responsible for maintaining and registering all hooks that
     * power the plugin.
     *
     * @since   1.0.0
     * @access  protected
     * @var     Sccoaching_Lgpr_Loader  $loader
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since   1.0.0
     * @access  protected
     * @var     string  $plugin_name
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since   1.0.0
     * @access  protected
     * @var     string  $version
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since   1.0.0
     */
    public function __construct() {

        $this->plugin_name = 'sccoaching-lgpr';
        $this->version = '1.0.0';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();

    }

    /**
     * Load the required dependencies for this plugin
     *
     * Include the following files that make up the plugin;
     *
     * - Sccoaching_Lgpr_Loader. Orchestrated the hooks of the plugin.
     * - Sccoaching_Lgpr_i18n. Defines internationalization functionality.
     * - Sccoaching_Lgpr_Admin. Defines all hooks for the admin area.
     * - Sccoaching_Lgpr_Public. Defines all hooks for the public side of the sire.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since   1.0.0
     * @access  private
     */
    private function load_dependencies() {

        /**
         * The class responsible for orchestrating the action and fitness of the
         * core plugin
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Sccoaching_Lgpr_Loader.php';

        /**
         * Tje class responsiebl for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Sccoaching_Lgpr_i18n.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/Sccoaching_Lgpr_Admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/Sccoaching_Lgpr_Public.php';

        $this->loader = new Sccoaching_Lgpr_Loader();
    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Sccoaching_Lgpr_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since   1.0.0
     * @access  private
     */
    private function set_locale() {

        $plugin_i18n = new Sccoaching_Lgpr_i18n();
        $plugin_i18n->set_domain( $this->get_plugin_name() );

        $this->loader->add_action( "plugins_loaded", $plugin_i18n, "load_plugin_textdomain" );

    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since   1.0.0
     * @access  private
     */
    private function define_admin_hooks() {

        $plugin_admin = new Sccoaching_Lgpr_Admin( $this->get_plugin_name(), $this->get_version() );

        // For assets
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
        $this->loader->add_action( 'admin_menu', $plugin_admin, 'add_menu_item' );
        $this->loader->add_action( 'admin_menu', $plugin_admin, 'add_submenu_item' );

        // Register REST ROUTES
        $this->loader->add_action( 'rest_api_init', $plugin_admin, 'define_rest_route' );

        // For localize
        $this->loader->add_action( 'wp_localize_script', $plugin_admin, 'localize_script' );

        // POST ROUTES
        // Add New Survey
        $this->loader->add_action( 'wp_ajax_nopriv_sccoaching_lgpr_add_report_template_form', $plugin_admin->report_template_controller, 'post_report_template' );
        $this->loader->add_action( 'wp_ajax_sccoaching_lgpr_add_report_template_form', $plugin_admin->report_template_controller, 'post_report_template' );

    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since   1.0.0
     * @access  private
     */
    private function define_public_hooks() {

        $plugin_public = new Sccoaching_Lgpr_Public( $this->get_plugin_name(), $this->get_version() );

        // For assets
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

        // Register Localize
        $this->loader->add_action( 'wp_localize_script', $plugin_public, 'localize_script' );

        // GET ROUTES
        $this->loader->add_shortcode( 'sccoaching_lgpr_survey', [$plugin_public->survey_controller, 'get_survey'] );

        // POST ROUTES
        // Add New Survey
        $this->loader->add_action( 'wp_ajax_nopriv_sccoaching_lgpr_add_survey_form', $plugin_public->survey_controller, 'post_survey' );
        $this->loader->add_action( 'wp_ajax_sccoaching_lgpr_add_survey_form', $plugin_public->survey_controller, 'post_survey' );
        // Delete Survey
        $this->loader->add_action( 'wp_ajax_nopriv_sccoaching_lgpr_delete_survey_form', $plugin_public->survey_controller, 'delete_survey' );
        $this->loader->add_action( 'wp_ajax_sccoaching_lgpr_delete_survey_form', $plugin_public->survey_controller, 'delete_survey' );
        // Edit Survey
        $this->loader->add_action( 'wp_ajax_nopriv_sccoaching_lgpr_edit_survey_form', $plugin_public->survey_controller, 'edit_survey' );
        $this->loader->add_action( 'wp_ajax_sccoaching_lgpr_edit_survey_form', $plugin_public->survey_controller, 'edit_survey' );

    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since   1.0.0
     */
    public function run() {

        $this->loader->run();

    }

    /**
     * The name of the plubin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since   1.0.0
     * @return  string
     */
    public function get_plugin_name() {

        return $this->plugin_name;

    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since   1.0.0
     * @return  Sccoaching_Lgpr
     */
    public function get_loader() {

        return $this->loader;

    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since   1.0.0
     * @return  string
     */
    public function get_version() {

        return $this->version;

    }
}