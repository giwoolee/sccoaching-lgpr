<?php

/**
 * Manages the database.
 *
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Database
{
    /**
     * @since 1.0.0
     * @access private
     * @var object
     */
    private $survey_migration;

    /**
     * @since 1.0.0
     * @access private
     * @var object
     */
    private $report_template_migration;

    /***
     * @since 1.0.0
     * @access private
     * @var string
     */
    private static $version = '1.0';

    /**
     * @since 1.0.0
     * @access private
     * @var string
     */
    private static $option_name = 'sccoaching_lgpr_db_version';

    /**
     * Constructor.
     */
    public function __construct() {

        global $wpdb;
        $prefix = $wpdb->prefix;
        $scc_prefix = 'sccoaching_lgpr_';

        $this->load_dependencies();

        // Migrations
        $this->survey_migration = new Sccoaching_Lgpr_Survey_Migration( $prefix, $scc_prefix );
        $this->report_template_migration = new Sccoaching_Lgpr_Report_Template_Migration( $prefix, $scc_prefix );

    }

    /**
     * Installs the tables needed by this plugin.
     */
    public function migrate() {

        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = $this->survey_migration->definition( $charset_collate );
        $sql .= $this->report_template_migration->definition( $charset_collate );

        dbDelta( $sql );

        add_option( self::$option_name, self::$version );

    }

    /**
     * Drop the tables used by this plugin.
     */
    public function remove() {

        global $wpdb;
        $wpdb->query( $this->survey_migration->reverse() );

        // Remove options
        delete_option( self::$option_name );

        // For site options in Multisite
        delete_site_option( self::$option_name );

    }

    /**
     * Load the dependencies for this class.
     */
    private function load_dependencies() {

        /**
         * The class for survey migration.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'migrations/Sccoaching_Lgpr_Survey_Migration.php';

        /**
         * The class for survey migration.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'migrations/Sccoaching_Lgpr_Report_Template_Migration.php';

        /**
         * The class for states seeder.
         */
        /* require_once plugin_dir_path( dirname( __FILE__ ) ) . 'seeders/class-coach-directory-states-seeder.php'; */

        /**
         * Class responsible for database table inserts.
         */
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    }
}