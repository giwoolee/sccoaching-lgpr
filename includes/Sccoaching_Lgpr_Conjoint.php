<?php

/**
 * Combination of miscellaneous functions.
 *
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Conjoint
{
    /**
     * Flattens the array.
     *
     * @param array $arr
     * @return array
     */
    public static function array_flatten( $arr = [] ) {

        $iter = new RecursiveIteratorIterator(new RecursiveArrayIterator( $arr ));
        $flat_arr = [];

        foreach ($iter as $item) {
            $flat_arr[] = $item;
        }

        return $flat_arr;

    }

    /**
     * Replace backslash quotes with the entity representation.
     *
     * @param string $str
     * @return mixed|string
     */
    public static function replace_quotes( $str = '' ) {
        $new_str = str_replace('\&quot;', '&quot;', $str);
        $new_str = str_replace("\\'", '&#039;', $new_str);
        return $new_str;
    }

    /**
     * Determines if the user is admin.
     *
     * @return bool
     */
    public static function is_user_admin() {
        return current_user_can( 'manage_options' );
    }
}