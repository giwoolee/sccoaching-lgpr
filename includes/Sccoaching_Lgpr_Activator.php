<?php

require_once 'Sccoaching_Lgpr_Database.php';

/**
 * Fired during plugin activation
 *
 * This class defines all code necessary to run during this plugin's actions.
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Activator
{
    /**
     * Activate the plugin.
     *
     * @since    1.0.0
     */
    public static function activate() {

        $database = new Sccoaching_Lgpr_Database();

        // Migrate data.
        $database->migrate();

    }

    /**
     * Install the plugin data.
     *
     * @since    1.0.0
     */
    /*public static function install_data() {

        $database = new Sccoaching_Lgpr_Database();

        // Seed table data.
        $database->seed();

    }*/
}