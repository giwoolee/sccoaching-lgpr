<?php

/**
 * Manages data input.
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Input
{
    /**
     * Retrieve post or get data.
     *
     * @param string $key
     * @param bool $sanitize
     * @return null|string
     */
    public static function get( $key = '', $sanitize = true ) {

        if (array_key_exists( $key, $_POST )) {
            $val = $_POST[$key];
            return $sanitize ? htmlspecialchars( $val ) : $val;
        }

        if (array_key_exists( $key, $_GET )) {
            $val = $_GET[$key];
            return $sanitize ? htmlspecialchars( $val ) : $val;
        }

        return null;

    }

    /**
     * Determines if the input exists.
     *
     * @param string $key
     * @param bool|true $sanitize
     * @return bool
     */
    public static function exists( $key = '', $sanitize = true ) {
        return !empty(self::get( $key, $sanitize ));
    }

    /**
     * Determines if a file is being uploaded.
     *
     * @param string $index
     * @return bool
     */
    public static function file_exists( $index = '' ) {
        return array_key_exists( $index, $_FILES ) && strlen( $_FILES[$index]['name'] ) > 0;
    }

    /**
     * Uploads a file.
     *
     * @param string $index
     * @param string $file_path
     * @return string
     */
    public static function upload_file($index = '', $file_path = 'public/image/') {

        $tmp_name = $_FILES[$index]['tmp_name'];
        $image = $_FILES[$index]['name'];

        $result = explode('.', $image);
        $fileext = $result[count($result)-1];

        list($usec, $sec) = explode(' ', microtime());
        $e_num = 1000 * ((float)$usec + (float)$sec);
        $realfilename = $e_num.'.'.$fileext;

        $base_path = plugin_dir_path( dirname( __FILE__ ) );
        move_uploaded_file($tmp_name, $base_path . $file_path . $realfilename);
        return $realfilename;

    }
}