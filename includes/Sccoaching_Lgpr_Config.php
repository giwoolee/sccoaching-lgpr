<?php

/**
 * Configuration settings interface
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/controllers
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Config
{
    /**
     * Configuration variables.
     *
     * @var array
     */
    private $conf = [];

    /**
     * Coach_Directory_Config constructor.
     */
    public function __construct() {
        $this->conf = include plugin_dir_path( dirname( __FILE__ ) ) . 'config/settings.php';
    }


    /**
     * Gets the specified dot delimited key.
     *
     * @param string $key
     * @return mixed
     */
    public function get( $key = '' ) {

        if ($key) {
            $keys = explode( '.', $key );
            return $this->resolve( $keys, $this->conf );
        }

        return null;

    }

    /**
     * Does the actual searching.
     *
     * @param array $keys
     * @param array $search
     * @return mixed
     */
    private function resolve( $keys = [], $search = [] ) {

        $ckey = array_shift( $keys );
        if (array_key_exists( $ckey, $search )) {
            $needle = $search[$ckey];
            if ( is_array( $needle ) && sizeof( $keys ) > 0 ) {
                return $this->resolve( $keys, $needle );
            } else {
                return $needle;
            }
        }

        return null;

    }
}