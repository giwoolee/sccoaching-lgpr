<?php

/**
 * Register all actions and filter for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run funtion to execute the list of actions and filters.
 *
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Loader
{
    /**
     * The array of actions registered with WordPress.
     *
     * @since   1.0.0
     * @access  protected
     * @var     array   @actions
     */
    protected $actions;

    /**
     * The array of filters registered with WordPress.
     *
     * @since   1.0.0
     * @access  protected
     * @var     array   $filters
     */
    protected $filters;

    /**
     * Initialize the collcetion used to maintain the actions and filters.
     *
     * @since   1.0.0
     */
    public function __construct() {

        $this->actions = [];
        $this->filters = [];
        $this->shortcodes = [];

    }

    /**
     * Add a new action to the collection to be registered with WordPRess.
     *
     * @since   1.0.0
     * @param   string  $hook
     * @param   object  $component
     * @param   string  $callback
     * @param   int     $priority
     * @param   int     $accepted_args
     */
    public function add_action( $hook, $component, $callback, $priority = 10, $accepted_args = 1 ) {
        $this->actions = $this->add( $this->actions, $hook, $component, $callback, $priority, $accepted_args );
    }

    /**
     * Add a new filter to the collection to be registered with WordPress.
     *
     * @since    1.0.0
     * @param      string               $hook             The name of the WordPress filter that is being registered.
     * @param      object               $component        A reference to the instance of the object on which the filter is defined.
     * @param      string               $callback         The name of the function definition on the $component.
     * @param      int                  $priority         The priority at which the function should be fired.
     * @param      int                  $accepted_args    The number of arguments that should be passed to the $callback.
     */
    public function add_filter( $hook, $component, $callback, $priority = 10, $accepted_args = 1 ) {
        $this->filters = $this->add( $this->filters, $hook, $component, $callback, $priority, $accepted_args );
    }

    /**
     * Add a new shortcode to the collection to be registered with WordPress.
     *
     * @param string $tag
     * @param string $func
     */
    public function add_shortcode( $tag, $func ) {
        $this->shortcodes[] = ['tag' => $tag, 'func' => $func];
    }

    /**
     * A utility function that is used to register the actions and hooks into a single
     * collection.
     *
     * @since    1.0.0
     * @access   private
     * @param      array                $hooks            The collection of hooks that is being registered (that is, actions or filters).
     * @param      string               $hook             The name of the WordPress filter that is being registered.
     * @param      object               $component        A reference to the instance of the object on which the filter is defined.
     * @param      string               $callback         The name of the function definition on the $component.
     * @param      int                  $priority         The priority at which the function should be fired.
     * @param      int                  $accepted_args    The number of arguments that should be passed to the $callback.
     * @return   array                                   The collection of actions and filters registered with WordPress.
     */
    private function add( $hooks, $hook, $component, $callback, $priority, $accepted_args ) {

        $hooks[] = array(
            'hook'          => $hook,
            'component'     => $component,
            'callback'      => $callback,
            'priority'      => $priority,
            'accepted_args' => $accepted_args
        );

        return $hooks;

    }

    /**
     * Register the filters and actions with WordPress.
     *
     * @since    1.0.0
     */
    public function run() {

        foreach ( $this->filters as $hook ) {
            add_filter( $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );
        }

        foreach ( $this->actions as $hook ) {
            add_action( $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );
        }

        foreach ( $this->shortcodes as $shortcode ) {
            add_shortcode( $shortcode['tag'], $shortcode['func'] );
        }

    }
}