<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Country</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <?php foreach ($surveys as $survey): ?>
    <tbody>
        <tr>
            <th scope="row"><?php echo $survey->id ?></th>
            <td><?php echo $survey->name ?></td>
            <td><?php echo $survey->country ?></td>
            <td><button class="edit_survey_btn"
                        data-id="<?php echo $survey->id ?>"
                        data-name="<?php echo $survey->name ?>"
                        data-country="<?php echo $survey->country?>">Edit</button></td>
            <td><button class="delete_survey_btn" data-id="<?php echo $survey->id ?>">Delete</button></td>
            <td><button class="export_pdfpreview_survey_btn"
                        data-id="<?php echo $survey->id ?>"
                        data-name="<?php echo $survey->name ?>"
                        data-country="<?php echo $survey->country?>">Preview</button>
            </td>
        </tr>
    </tbody>
    <?php endforeach; ?>
</table>
<br>
<div class="alert" style="color: red;" id="response_survey"></div>