<?php

require_once 'Sccoaching_Lgpr_Base_Controller.php';

/**
 * Plugin User Controller
 *
 * The class responsible for controlling the routes to views.
 *
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/controllers
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Survey_Controller extends Sccoaching_Lgpr_Base_Controller
{
    /**
     * @since 1.0.0
     * @access private
     * @var int
     */
    private $config;

    /**
     * Coach_Directory_User_Controller constructor.
     */
    public function __construct() {

        $this->load_dependencies();
        $this->config = new Sccoaching_Lgpr_Config();

    }

    /**
     * Survey View
     */
    public function get_survey() {

        $survey = new Sccoaching_Lgpr_Survey_Model();
        $this->render('engagement_admin/index', [
            'surveys' => $survey->survey_list_all()
        ]);

    }

    /**
     * Survey update
     */
    public function post_survey() {

        // Post survey
        $survey = new Sccoaching_Lgpr_Survey_Model();
        $survey->set( 'name', Sccoaching_Lgpr_Conjoint::replace_quotes( Sccoaching_Lgpr_Input::get( 'name' ) ) )
            ->set( 'country', Sccoaching_Lgpr_Conjoint::replace_quotes( Sccoaching_Lgpr_Input::get( 'country' ) ) );
        $survey->save();

        // Using die() for WP Asynchronous call.
        die();

    }

    /**
     * Delete survey
     */
    public function delete_survey() {

        // Delete the survey
        $survey = new Sccoaching_Lgpr_Survey_Model();
        $survey->delete_by_id(Sccoaching_Lgpr_Input::get( 'id' ));

        // Using die() for WP Asynchronous call.
        die();

    }

    /**
     * Update survey
     */
    public function update_survey() {

        // Using die() for WP Asynchronous call.
        die();

    }

    /**
     * Load the dependencies for this class.
     */
    private function load_dependencies() {
        /**
         * The class responsible for profile model.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'models/Sccoaching_Lgpr_Survey_Model.php';

        /**
         * The class responsible for managing input.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Sccoaching_Lgpr_Input.php';

        /**
         * The class responsible for conjoint methods.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Sccoaching_Lgpr_Conjoint.php';

        /**
         * The class responsible for configuration settings.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Sccoaching_Lgpr_Config.php';

    }
}