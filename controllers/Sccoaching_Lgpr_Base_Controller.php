<?php

/**
 * Plugin Base Controller
 *
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/controllers
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
abstract class Sccoaching_Lgpr_Base_Controller
{
    /**
     * Render a view page.
     *
     * @param string $path
     * @param array $data
     * @param bool|false $exit
     */
    protected function render( $path = '', $data = [], $exit = false ) {

        // Expose the variables
        foreach ($data as $k => $v)
            ${$k} = $v;

        include plugin_dir_path( dirname( __FILE__ ) ) . 'views/' . $path .  '.php';

        if ($exit)
            exit();

    }

    /**
     * Determine if we are posting.
     *
     * @return bool
     */
    protected function is_post() {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }

}