<?php

require_once 'Sccoaching_Lgpr_Base_Controller.php';

/**
 * Plugin User Controller
 *
 * The class responsible for controlling the routes to views.
 *
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/controllers
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Report_Template_Controller extends Sccoaching_Lgpr_Base_Controller
{
    /**
     * @since 1.0.0
     * @access private
     * @var int
     */
    private $config;

    /**
     * Coach_Directory_User_Controller constructor.
     */
    public function __construct() {

        $this->load_dependencies();
        $this->config = new Sccoaching_Lgpr_Config();

    }

    /**
     * Get latest created report template (order by 'created_at' desc)
     */
     public function get_latest_report_template() {

        $report_template = new Sccoaching_Lgpr_Report_Template_Model();

        /* return $report_template->report_template_get_latest(); */

        $this->render(
            'report_template',
            [
                'report_template'  =>  $report_template
            ]
        );

     }

    /**
     * Post report template
     */
     public function post_report_template() {

        // Post post_template
        $template = new Sccoaching_Lgpr_Report_Template_Model();

        // Prepare statement to be saved
        $template->set( 'header_description', Sccoaching_Lgpr_Conjoint::replace_quotes( Sccoaching_Lgpr_Input::get('report-header-description') ) )
            ->set( 'questionnaire', Sccoaching_Lgpr_Conjoint::replace_quotes( Sccoaching_Lgpr_Input::get('report-questionnaire') ) )
            ->set( 'coaches', Sccoaching_Lgpr_Conjoint::replace_quotes( Sccoaching_Lgpr_Input::get('report-footer-coaches') ) )
            ->set( 'cities', Sccoaching_Lgpr_Conjoint::replace_quotes( Sccoaching_Lgpr_Input::get('report-footer-cities') ) )
            ->set( 'countries', Sccoaching_Lgpr_Conjoint::replace_quotes( Sccoaching_Lgpr_Input::get('report-footer-countries') ) )
            ->set( 'languages', Sccoaching_Lgpr_Conjoint::replace_quotes( Sccoaching_Lgpr_Input::get('report-footer-languages') ) )
            ->set( 'process', Sccoaching_Lgpr_Conjoint::replace_quotes( Sccoaching_Lgpr_Input::get('report-footer-process') ) )
            ->set( 'email', Sccoaching_Lgpr_Conjoint::replace_quotes( Sccoaching_Lgpr_Input::get('report-footer-email') ) )
            ->set( 'website', Sccoaching_Lgpr_Conjoint::replace_quotes( Sccoaching_Lgpr_Input::get('report-footer-website') ) )
            ->set( 'created_at', date("Y-m-d H:i:s") );

        // Save report template to DB
        $template->save();

        // Using die() for WP Asynchronous call.
        die();

     }

    /**
     * Load the dependencies for this class.
     */
    private function load_dependencies() {
        /**
         * The class responsible for profile model.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'models/Sccoaching_Lgpr_Report_Template_Model.php';

        /**
         * The class responsible for managing input.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Sccoaching_Lgpr_Input.php';

        /**
         * The class responsible for conjoint methods.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Sccoaching_Lgpr_Conjoint.php';

        /**
         * The class responsible for configuration settings.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Sccoaching_Lgpr_Config.php';

    }
}