<?php

require_once 'Sccoaching_Lgpr_Base_Migration.php';

/**
 * Report Template Migration
 *
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Report_Template_Migration extends Sccoaching_Lgpr_Base_Migration
{
    /**
     * Sccoaching_Lgpr_Report_Template_Migration
     *
     * @param string @prefix
     * @param string @scc_prefix
     */
    public function __construct( $prefix = '', $scc_prefix = '' ) {

        parent::__construct( $prefix, $scc_prefix );
        $this->table = $prefix . $scc_prefix . 'report_template';

    }

    /**
     * Return the table definition.
     *
     * @param string $collate
     * @return string
     */
    public function definition( $collate = '' ) {
        return "CREATE TABLE $this->table (
            id int(11) unsigned NOT NULL AUTO_INCREMENT,
            PRIMARY KEY  id (id),
            header_description varchar(256) NOT NULL,
            questionnaire varchar(256) NOT NULL,
            coaches varchar(4) NOT NULL,
            cities varchar(3) NOT NULL,
            countries varchar(3) NOT NULL,
            languages varchar(3) NOT NULL,
            process varchar(2) NOT NULL,
            email varchar(30) NOT NULL,
            website varchar(30) NOT NULL,
            created_at datetime  NOT NULL
        ) $collate;";
    }
}