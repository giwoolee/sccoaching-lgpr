<?php

require_once 'Sccoaching_Lgpr_Base_Migration.php';

/**
 * Testing DB
 *
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
class Sccoaching_Lgpr_Survey_Migration extends Sccoaching_Lgpr_Base_Migration
{
    /**
     * Sccoaching_Lgpr_Survey
     *
     * @param string @prefix
     * @param string @scc_prefix
     */
    public function __construct( $prefix = '', $scc_prefix = '' ) {

        parent::__construct( $prefix, $scc_prefix );
        $this->table = $prefix . $scc_prefix . 'survey';

    }

    /**
     * Return the table definition.
     *
     * @param string $collate
     * @return string
     */
    public function definition( $collate = '' ) {

        return "CREATE TABLE $this->table (
          id int(11) unsigned NOT NULL AUTO_INCREMENT,
          name varchar(50) NOT NULL,
          country varchar(20) NOT NULL,
          PRIMARY KEY  id (id),
          UNIQUE KEY name (name)
        ) $collate;";
    }
}