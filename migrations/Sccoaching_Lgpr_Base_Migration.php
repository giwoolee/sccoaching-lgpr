<?php

/**
 * Base Migration
 *
 *
 * @since       1.0.0
 * @package     Sccoaching_Lgpr
 * @subpackage  Sccoaching_Lgpr/includes
 * @author      [Work E-mail] Gustavo Lee <ggl@sccoaching.com>
 *              [Personal E-mail] Gustavo Lee <gglee89@gmail.com>
 *
 */
abstract class Sccoaching_Lgpr_Base_Migration
{
    /**
     * @since 1.0.0
     * @access protected
     * @var string
     */
    protected $table;

    /**
     * @since 1.0.0
     * @access protected
     * @var string
     */
    protected $prefix;

    /**
     * @since 1.0.0
     * @access protected
     * @var string
     */
    protected $scc_prefix;

    /**
     * Coach_Directory_Base_Migration constructor.
     *
     * @param string $prefix
     *
     */
    public function __construct( $prefix = '', $scc_prefix = '' ) {
        $this->prefix = $prefix;
        $this->scc_prefix = $scc_prefix;
    }

    /**
     * Get the table.
     *
     * @return string
     */
    public function get_table() {
        return $this->table;
    }

    /**
     * Get the table definition.
     *
     * @param string $collate
     * @return mixed
     */
    abstract public function definition( $collate = '' );

    /**
     * Get drop definition.
     *
     * @return mixed
     */
    public function reverse() {
        return "DROP TABLE IF EXISTS $this->table";
    }

}